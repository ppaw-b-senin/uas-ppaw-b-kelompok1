
import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Letter from './layout/Letter';
import Navbar1 from './layout/Navbar1';
import Edit from './layout/Edit';




import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function EditProfile() {
  return (
    
    <React.Fragment>
      <Navbar1/>
      <Edit/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default EditProfile
