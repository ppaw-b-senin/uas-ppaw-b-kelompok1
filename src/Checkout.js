import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Letter from './layout/Letter';
import Navbar1 from './layout/Navbar1';
import Co from './layout/Co';


import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function Checkout() {
  return (
    
    <React.Fragment>
      <Navbar1/>
      <Co/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default Checkout
