import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Letter from './layout/Letter';
import Navbarup from './layout/Navbarup';
import UploadPayment2 from './layout/UploadPayment2';



import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function UploadPaymentSukses() {
  return (
    
    <React.Fragment>
      <Navbarup/>
    	<UploadPayment2/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default UploadPaymentSukses
