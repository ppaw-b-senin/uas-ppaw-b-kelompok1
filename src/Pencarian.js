import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Letter from './layout/Letter';
import Hero from './layout/Hero';
import Pencarian1 from './layout/Pencarian1';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function Pencarian() {
  return (
    
    <React.Fragment>
      <Navbar/>
      <Hero/>
    	<Pencarian1/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default Pencarian
