import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Homeslider from './layout/Homeslider';
import Hand from './layout/Hand';
import Middlebanner1 from './layout/Middlebanner1';
import Deals from './layout/Deals';
import Partner from './layout/Partner';
import Letter from './layout/Letter';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
  return (
    <Router>
    <React.Fragment>
      <Route exact path ='/' component={Navbar}/>
      <Homeslider/>
      <Hand/>
      <Middlebanner1/>
      <Deals/>
      <Partner/>
      <Letter/>
    </React.Fragment>
    </Router>
  );
}

export default App;
