import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Hero from './layout/Hero';
import Letter from './layout/Letter';
import Booking from './layout/Booking';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function BookingDetail() {
  return (
    
    <React.Fragment>
      <Navbar/>
      <Hero/>
      <Booking/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default BookingDetail;
