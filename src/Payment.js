import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbarpay from './layout/Navbarpay';
import Payment1 from './layout/Payment1';

import Letter from './layout/Letter';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function Payment() {
  return (
    
    <React.Fragment>
      <Navbarpay/>
      <Payment1/>
 
  
      <Letter/>
    </React.Fragment>
    
  );
}

export default Payment;
