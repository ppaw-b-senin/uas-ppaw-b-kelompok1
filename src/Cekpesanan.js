import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Hero from './layout/Hero';
import Letter from './layout/Letter';
import Order from './layout/Order';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App2() {
  return (
    
    <React.Fragment>
      <Navbar/>
      <Hero/>
      <Order/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default App2;
