import React from 'react';
import iconmap from '../img/icon-map.png';
import iconcc from '../img/icon-cc.png';
import iconshut from '../img/icon-shut.png';
import iconprofile from '../img/icon-profile.png';
import lionair from '../img/lionair.png';
import plane from '../img/plane.png';
import train from '../img/train.png';
import hotels from '../img/hotels.png';
const Mabooking = () =>{
	return(

		<div className="settings booking">
    <div className="container">
      <div className="row">
        <div className="col-md-4 col-12">
          <div className="sidebar navigate">
            <h1>NAVIGATE</h1>
            <h3>LOG IN / Register</h3>
            <ul className="navigation">
              <li><a href="#"><img src={iconprofile} className="img-fluid"/> Edit Profile</a></li>
              <li><a href="#"><img src={iconcc} className="img-fluid"/> Purchase List</a> <span className="count">1</span></li>
              <li><a href="#" className="active"><img src={iconmap} className="img-fluid"/> My Booking</a></li>
              <li><a href="#"><img src={iconshut} className="img-fluid"/> Log Out</a></li>
            </ul>
          </div>
        </div>
        <div className="col">
          <nav>
            <div className="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
              <a className="nav-item nav-link plane active" id="nav-plane-tab" data-toggle="tab" href="#nav-plane" role="tab" aria-controls="nav-plane" aria-selected="true"><img src={plane} className="img-fluid"/> Tiket Pesawat</a>
              <a className="nav-item nav-link train" id="nav-train-tab" data-toggle="tab" href="#nav-train" role="tab" aria-controls="nav-train" aria-selected="false"><img src={train} className="img-fluid"/> Kereta Api</a>
              <a className="nav-item nav-link hotel" id="nav-hotel-tab" data-toggle="tab" href="#nav-hotel" role="tab" aria-controls="nav-hotel" aria-selected="false"><img src={hotels} className="img-fluid"/> Hotel</a>
            </div>
          </nav>
          <div className="tab-content" id="nav-tabContent">
            <div className="tab-pane fade show active" id="nav-plane" role="tabpanel" aria-labelledby="nav-plane-tab">
              <h3>Active Booking (1)</h3>
              <div className="list-content">
                <div className="area-to">
                  <p className="mb-0 tojoe"><strong className="tujuan">Jakarta (CGK) <i className="fa fa-long-arrow-right" aria-hidden="true"></i> Palembang(PLM)</strong></p>
                  <p className="text-right">Booking ID <strong className="tujuan">314507627</strong></p>
                </div>
                <div className="box-profile no-shadow">
                  <div className="purchase-info">
                    <h4 className="blue">FLIGHT SCHEDULE</h4>
                    <span className="desc">21 Des 2018 | 06:05</span>
                  </div>
                  <div className="purchase-info">
                    <h4 className="blue">FLIGHT SCHEDULE</h4>
                    <span className="desc">Soekarno Hatta International Airport</span>
                  </div>
                </div>
                <div className="next-information">
                  <div className="clock">
                    <button type="button" name="button" className="btn btn-blue">VERIFYING PAYMENT</button>
                  </div>
                  <div className="dropdown show">
                    <a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Details <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                      <a className="dropdown-item details-link" href="#" className="">See Details</a>
                      <a className="dropdown-item red remove-link" href="#" >Remove Purchase</a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="history">
                <h3>Booking History</h3>
                <div className="list-content">
                  <div className="area-to">
                    <p className="mb-0 tojoe"><strong className="tujuan">Jakarta (CGK) <i className="fa fa-long-arrow-right" aria-hidden="true"></i> Palembang (PLM)</strong></p>
                    <div className="logo-plane">
                      <img src={lionair} alt="lion air" className="img-fluid"/>
                      <span>Lion Air QG-980</span>
                    </div>
                  </div>
                  <div className="box-profile no-shadow">
                    <div className="purchase-info">
                      <h4 className="grey">FLIGHT SCHEDULE</h4>
                      <span className="desc">21 Des 2018 | 06:05</span>
                    </div>
                    <div className="purchase-info">
                      <h4 className="grey">FLIGHT SCHEDULE</h4>
                      <span className="desc">Soekarno Hatta International Airport</span>
                    </div>
                  </div>
                  <div className="next-information">
                    <div className="clock">
                      <button type="button" name="button" className="btn btn-blue bg-green">E-TICKET ISSUED</button>
                    </div>
                    <div className="dropdown show">
                      <a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Details <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
                      </a>
                      <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                        <a className="dropdown-item details-link" href="#" className="">See Details</a>
                        <a className="dropdown-item red remove-link" href="#" >Remove Purchase</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="seemore">
                <a href="#" className="blue">See more <i className="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
            <div className="tab-pane fade" id="nav-train" role="tabpanel" aria-labelledby="nav-train-tab">
              Booking Kereta
            </div>
            <div className="tab-pane fade" id="nav-hotel" role="tabpanel" aria-labelledby="nav-hotel-tab">
              Booking Hotel
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

		)
}

export default Mabooking