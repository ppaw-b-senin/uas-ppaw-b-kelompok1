import React from 'react';


const Co = () =>{
	return(
<React.Fragment>
  <div className="section checkout">
    <div className="container">
      <h1 className="title">Pemesanan</h1>
      <h2> Data Pemesanan</h2>
      <div className="row">
        <div className="col-md-7 col-12">
          <div className="box-form">
            <form className="" action="" method="post">
              <div className="form-group">
                <label for="">Nama Perusahaan</label>
                <input type="text" name="" value="" className="form-control"/>
                <span className="desc">Nama pemesanan sesuai KTP/SIM/Paspor</span>
              </div>
              <div className="form-row">
                <div className="col">
                  <label for="inputCity">No. Handphone</label>
                  <div className="form-row label-bottom">
                    <div className="form-group col-md-4">
                      <input type="text" className="form-control" id="inputID" value="+62"/>
                    </div>
                    <div className="form-group col-md-8">
                      <input type="text" className="form-control" id="inputNumber"/>
                    </div>
                    <span className="desc">Contoh: +62 812345678, untuk Kode Negara (+62) dan No. Handphone 0812345678</span>
                  </div>
                </div>
                <div className="form-group col">
                  <label for="inputCity">Email</label>
                  <input type="email" className="form-control" id="inputEmail"/>
                  <span className="desc">Contoh: email@example.com</span>
                </div>
              </div>
              <div className="action">
                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#Lanjutkan" >LANJUTKAN</button>
              </div>
            </form>
          </div>
        </div>
        <div className="col-md-5 col-12">
          <div className="box-information-input">
            <div className="box-title">
              <h2>Kereta Pergi</h2>
              <p>Rabu, 15 Agu 2018</p>
            </div>
            <div className="inner-information">
              <p className="name-train">Ceramai</p>
              <p className="blok-train">Bisnis (B)</p>
              <div className="list-route">
                <ul>
                  <li className="from">
                    <div className="block">
                      <span className="boarding">17:40</span>
                      <span className="date">Jul 27, 2018</span>
                    </div>
                    <div className="block">
                      <span className="boarding">Jakarta (GMR)</span>
                      <span className="date">Stasiun Gambir</span>
                    </div>
                  </li>
                  <li className="to">
                    <div className="block">
                      <span className="boarding">09:06</span>
                      <span className="date">Jul 28, 2018</span>
                    </div>
                    <div className="block">
                      <span className="boarding">Malang (ML</span>
                      <span className="date">Stasiun Malang</span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="checkout" id="BlockNext">
    <div className="container">
      <h2> Detail Penumpang</h2>
      <div className="row">
        <div className="col-md-7 col-12">
          <div className="warning-alert">
            <i className="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <ul>
              <li>- Passenger detils must be written as on ID card/passport/driving license</li>
              <li>- Passenger data cannot be changed after passing this page</li>
            </ul>
          </div>

          <div className="box-form">
            <form className="" action="" method="post">
              <h2> Detail Penumpang</h2>
              <div className="form-group">
                <div className="form-row">
                  <div className="col-md-3">
                    <label for="">Title</label>
                    <select className="custom-select">
                      <option value="1"><span className="circle"></span> Mr</option>
                      <option value="2"><span className="circle"></span> Mrs</option>
                      <option value="3"><span className="circle"></span> Miss</option>
                    </select>
                  </div>
                  <div className="col">
                    <label for="">Passenger Name</label>
                    <input type="text" name="" value="" className="form-control"/>
                    <span className="desc">Name as on ID card/passport/driving license</span>
                  </div>
                </div>
              </div>
              <div className="form-row">
                <div className="col-md-3">
                  <label for="">ID Type</label>
                  <select className="custom-select">
                    <option value="11">Passport</option>
                    <option value="21">KTP</option>
                    <option value="31">SIM</option>
                  </select>
                </div>
                <div className="col">
                  <label for="">ID number</label>
                  <input type="text" name="" value="" className="form-control"/>
                  <span className="desc">For passengers below 18 years old you may enter another valid ID number (e.g. birth certificate, student ID) or date of birth (ddmmyyyy)</span>
                </div>
              </div>
            </form>
          </div>

          <h2> Detail Harga</h2>
          <div className="box-form price">
            <div className="name-of-train">
              <div className="priority">
                <span>Gajayana Priority (Adult) x 1</span>
              </div>
              <div className="price-list">
                <span className="idr">Rp.</span>1.000.000
              </div>
            </div>
            <div className="footer-total">
              <div className="priority">
                <h1>Total</h1>
              </div>
              <div className="price-list">
                <span className="idr">Rp.</span>1.000.000
              </div>
            </div>
          </div>
          <div className="action mb-40 last">
            <button type="button" name="button" className="seat btn btn-grey">Select Seat</button>
            <span>Or</span>
            <button type="button" name="button" className="btn btn-blue" data-toggle="modal" data-target="#Payment">CONTINUE TO PAYMENT</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</React.Fragment>
  

		)
}

export default Co