import React from 'react';





const UploadPayment1 = () =>{
	return(

    <div className="section checkout">
    <div className="container">
      <h1 className="title">Upload Payment Proof</h1>
      <div className="row">
        <div className="col-md-8 col-12">
          <div className="content-wide">
            <div className="box-information-input">
                <div className="box-title">
                    <span className="block blue">Your booking status:</span>
                    <strong className="blue">Waiting for Payment Proof</strong>
                </div>
                <div className="inner-information text-center">
                    <div className="img-icon">
                        <i className="fa fa-upload" aria-hidden="true"></i>
                    </div>
                    <p className="informa-desc">
                        Please upload your payment proof to speed up the payment verification process.
                    </p>
                    <p className="desc">You may upload up to 3MB in PNG, JPG, or JPEG format.</p>
                    <div className="action text-center mb-10">
                        <button type="file" className="btn btn-blue">Upload Payment Proof</button>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-12">
          <div className="box-information-input">
            <div className="box-title">
              <h2>Booking Code</h2>
              <p className="code">312345667</p>
            </div>
            <div className="inner-information">
              <div className="top">
                <span>YOUR TRIP</span>
                <a href="payment-detail.html" className="blue">Details</a>
              </div>
              <div className="middle">
                <p className="name">Flight</p>
                <span className="desc">Dec 21, 2018</span>
                <p className="flight"><i className="fa fa-circle" aria-hidden="true"></i><span>Jakarta (CKG) </span> <i className="fa fa-long-arrow-right" aria-hidden="true"></i> <span>Palembang (PLM)</span></p>
              </div>
            </div>
            <div className="passenger">
              <h4>LIST OF PASSENGER (S)</h4>
            </div>
            <div className="list-of">
              <strong>Miss Novita Hidayati</strong> <span><strong>Adult</strong></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

		)
}

export default UploadPayment1