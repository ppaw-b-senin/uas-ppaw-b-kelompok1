import React from 'react';
import iconmap from '../img/icon-map.png';
import iconcc from '../img/icon-cc.png';
import iconshut from '../img/icon-shut.png';
import iconprofile from '../img/icon-profile.png';


const Edit = () =>{
	return(

		<div className="settings">
    <div className="container">
      <div className="row">
        <div className="col-md-4 col-12">
          <div className="sidebar navigate">
            <h1>NAVIGATE</h1>
            <h3>LOG IN / Register</h3>
            <ul className="navigation">
              <li><a href="#" className="active"><img src={iconprofile} className="img-fluid"/> Edit Profile</a></li>
              <li><a href="#" ><img src={iconcc} className="img-fluid"/> Purchase List</a> <span className="count">1</span></li>
              <li><a href="#"><img src={iconmap} className="img-fluid"/> My Booking</a></li>
              <li><a href="#"><img src={iconshut} className="img-fluid"/> Log Out</a></li>
            </ul>
          </div>
        </div>
        <div className="col">
          <div className="content-bar">
            <div className="box-profile set-name">
              <span className="name"><i className="fa fa-user" aria-hidden="true"></i> Novita Hidayati</span>
              <a href="#" className="btn btn-grey" id="Update"><i className="fa fa-pencil-square-o" aria-hidden="true"></i> Update Profile</a>
              <div className="form-name" id="EditName">
                <form className="" action="" method="post">
                  <div className="form-group">
                    <label for="">Full name</label>
                    <input type="text" name="" value="" className="form-control"/>
                    <span className="desc">As appears on identity card (without punctuation mark)</span>
                  </div>
                  <div className="action">
                    <button type="button" name="button" className="btn btn-grey" id="CloseBox">Cancel</button>
                    <button type="button" name="button" className="btn btn-blue">Save</button>
                  </div>
                </form>
              </div>
            </div>
            <div className="box-profile set-phone">
              <span className="name"><i className="fa fa-mobile" aria-hidden="true"></i> Phone</span>
              <a href="/Nohand" className="btn btn-blue" data-toggle="modal" data-target="#IDLogin"><i className="fa fa-pencil-square-o" aria-hidden="true"></i> Add Phone</a>
              <span className="desc">Mobile number to receive account-related notification.</span>
            </div>
            <div className="box-profile set-mail">
              <div className="inner">
                <span className="name"><i className="fa fa-envelope" aria-hidden="true"></i> Email Address</span>
                <a href="/Email" className="btn btn-blue" data-toggle="modal" data-target="#AddEmail"><i className="fa fa-pencil-square-o" aria-hidden="true"></i> Add Email Address</a>
                <span className="desc">Email address to receive account-related notification.</span>
              </div>
              <div className="footer-notif">
                <span className="email">1. novitahidayati52@gmail.com</span>
                <span className="green">Recipient for notifications</span>
                <a href="#" className="delete"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

		)
}

export default Edit