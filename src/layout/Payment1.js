import React from 'react';
import bca from '../img/bca.png';
import bni from '../img/bni.png';
import bri from '../img/bri.png';
import mandiri from '../img/mandiri.png';

const Payment1 = () =>{
	return(

       <div className="section checkout">
    <div className="container">
      <h1 className="title">Pembayaran</h1>
      <h2> Data Pemesanan</h2>
      <div className="row">
        <div className="col-md-8 col-12">
          <div className="content-wide">
            <div className="row">
              <div className="col-6 bg-blue">
                <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                  <div className="title">PREFERRED METHOD</div>
                  <a className="nav-link active" id="v-pills-one-tab" data-toggle="pill" href="#v-pills-one" role="tab" aria-controls="v-pills-one" aria-selected="true">Transfer</a>
                  <a className="nav-link" id="v-pills-two-tab" data-toggle="pill" href="#v-pills-two" role="tab" aria-controls="v-pills-two" aria-selected="false">Credit Card<br/><span>Up to 12x Installments available from 9 Banks</span></a>
                  <a className="nav-link" id="v-pills-three-tab" data-toggle="pill" href="#v-pills-three" role="tab" aria-controls="v-pills-three" aria-selected="false">ATM</a>
                  <a className="nav-link" id="v-pills-four-tab" data-toggle="pill" href="#v-pills-four" role="tab" aria-controls="v-pills-four" aria-selected="false">CIMB Cliks</a>
                  <a className="nav-link" id="v-pills-five-tab" data-toggle="pill" href="#v-pills-five" role="tab" aria-controls="v-pills-five" aria-selected="false">BCA Klikpay</a>
                  <a className="nav-link" id="v-pills-six-tab" data-toggle="pill" href="#v-pills-six" role="tab" aria-controls="v-pills-six" aria-selected="false">Mandiri Debit</a>
                  <a className="nav-link" id="v-pills-seven-tab" data-toggle="pill" href="#v-pills-seven" role="tab" aria-controls="v-pills-seven" aria-selected="false">Indomaret</a>
                  <a className="nav-link" id="v-pills-eight-tab" data-toggle="pill" href="#v-pills-eight" role="tab" aria-controls="v-pills-eight" aria-selected="false">Alfamart</a>
                </div>
              </div>
              <div className="col-6 box">
                <div className="tab-content" id="v-pills-tabContent">
                  <div className="tab-pane fade show active" id="v-pills-one" role="tabpanel" aria-labelledby="v-pills-one-tab">
                    <div className="box-method">
                      <div className="title">Complete payment in 03:54:44</div>
                      <h5>Transfer</h5>
                      <div className="alert-box"><i className="fa fa-info-circle"></i> You can transfer from any banking channel (m-Banking, SMS Banking or ATM)</div>
                      <p className="select">Select a Destination Account</p>
                      <div className="radio">
                        <div className="custom-control custom-radio">
                          <input type="radio" id="customRadio1" name="customRadio" className="custom-control-input"/>
                          <label className="custom-control-label" for="customRadio1">BCA <div className="img"><img src={bca} className="img-fluid"/></div></label>
                        </div>
                        <div className="custom-control custom-radio">
                          <input type="radio" id="customRadio2" name="customRadio" className="custom-control-input"/>
                          <label className="custom-control-label" for="customRadio2">Mandiri <div className="img"><img src={mandiri} className="img-fluid"/></div></label>
                        </div>
                        <div className="custom-control custom-radio">
                          <input type="radio" id="customRadio3" name="customRadio" className="custom-control-input"/>
                          <label className="custom-control-label" for="customRadio3">BRI <div className="img"><img src={bri} className="img-fluid"/></div></label>
                        </div>
                        <div className="custom-control custom-radio">
                          <input type="radio" id="customRadio4" name="customRadio" className="custom-control-input"/>
                          <label className="custom-control-label" for="customRadio4">BNI <div className="img"><img src={bni} className="img-fluid"/></div></label>
                        </div>
                      </div>
                      <div className="check-slide">
                        <div className="custom-control custom-checkbox">
                          <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                          <label className="custom-control-label" for="customCheck1">Add Coupon</label>
                        </div>
                        <div className="custom-control custom-checkbox redeem">
                          <input type="checkbox" className="custom-control-input" id="customCheck2"/>
                          <label className="custom-control-label" for="customCheck2">Redeem Points</label>
                          <span className="desc">71 Points</span>
                          <span className="max desc">Minimum of 2500 Points required</span>
                        </div>
                      </div>
                      <div className="price-detil">
                        <h6>Price Details</h6>
                        <div className="table">
                          <table className="table">
                            <tr>
                              <td>Citilink (Adult) x 1</td>
                              <td>Rp. 438.500</td>
                            </tr>
                            <tr>
                              <td>Baggage CGK-PLM</td>
                              <td className="green">FREE</td>
                            </tr>
                            <tr>
                              <td>Unique Code</td>
                              <td className="red">Rp. -822</td>
                            </tr>
                            <tr className="footer">
                              <td>Total Price</td>
                              <td>Rp. 437.672</td>
                            </tr>
                          </table>
                        </div>
                      </div>
                      <div className="bg-blue">
                        <span>this account (novitahidayati52@gmail.com) will earn 35 points</span>
                      </div>
                      <div className="footer-box">
                        <p>by clicking the button below, you agree to <a href="#" className="blue">pasarprodukbumn's Terms & Condition</a> and <a href="#" className="blue">Privacy Policy</a></p>
                      </div>
                      <div className="action">
                        <button type="button" className="btn btn-blue">Pay With Transfer</button>
                      </div>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="v-pills-two" role="tabpanel" aria-labelledby="v-pills-two-tab">...</div>
                  <div className="tab-pane fade" id="v-pills-three" role="tabpanel" aria-labelledby="v-pills-three-tab">...</div>
                  <div className="tab-pane fade" id="v-pills-four" role="tabpanel" aria-labelledby="v-pills-four-tab">...</div>
                  <div className="tab-pane fade" id="v-pills-five" role="tabpanel" aria-labelledby="v-pills-five-tab">...</div>
                  <div className="tab-pane fade" id="v-pills-six" role="tabpanel" aria-labelledby="v-pills-six-tab">...</div>
                  <div className="tab-pane fade" id="v-pills-seven" role="tabpanel" aria-labelledby="v-pills-seven-tab">...</div>
                  <div className="tab-pane fade" id="v-pills-eight" role="tabpanel" aria-labelledby="v-pills-eight-tab">...</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-12">
          <div className="box-information-input">
            <div className="box-title">
              <h2>Booking Code</h2>
              <p className="code">312345667</p>
            </div>
            <div className="inner-information">
              <div className="top">
                <span>YOUR TRIP</span>
                <a href="payment-detail.html" className="blue">Details</a>
              </div>
              <div className="middle">
                <p className="name">Flight</p>
                <span className="desc">Dec 21, 2018</span>
                <p className="flight"><i className="fa fa-circle" aria-hidden="true"></i><span>Jakarta (CKG) </span> <i className="fa fa-long-arrow-right" aria-hidden="true"></i> <span>Palembang (PLM)</span></p>
              </div>
            </div>
            <div className="passenger">
              <h4>LIST OF PASSENGER (S)</h4>
            </div>
            <div className="list-of">
              <strong>Miss Novita Hidayati</strong> <span><strong>Adult</strong></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
		
		)
}

export default Payment1