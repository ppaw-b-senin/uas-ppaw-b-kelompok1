import React from 'react';
import logo from '../img/logo.jpg';
import fbsquare from '../img/fb-square.png';
import gpsquare from '../img/gp-square.png';
import ind from '../img/ind.png';
import eng from '../img/eng.png';
import {Link} from 'react-router-dom';


const Navbar = () =>{
	return(
		<nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <a className="navbar-brand" href="/App">
          <img src={logo} alt="main logo" className="img-fluid"/>
        </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav flex-row ml-md-auto d-md-flex">
            <li className="nav-item active">
              <a className="nav-link" href="/Bantuan">Bantuan</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Promo</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#"><i className="fa fa-shopping-cart" aria-hidden="true"></i> Keranjang</a>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-user-circle-o" aria-hidden="true"></i> Masuk</a>
              <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                <div className="header-dropdown">
                  <h3>Log In ke Akun Anda</h3>
                </div>
                <div className="form-login">
                  <form className="" action="" method="post">
                    <div className="form-group">
                      <label for="">Email atau no handphone</label>
                      <input type="text" name="" value="" className="form-control"/>
                    </div>
                    <div className="form-group">
                      <label for="">Password</label>
                      <a href="#" className="forgot">Lupa Password <i className="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                      <input type="password" name="" value="" className="form-control"/>
                    </div>
                    <div className="button">
                      <button type="submit" name="button" className="btn btn-blue">Log In</button>
                      <div className="reg">
                        <p className="ask">Belum punya akun?</p>
                        <a href="#">Daftar <i className="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="other-log">
                  <p className="text-center">Atau masuk dengan:</p>
                  <button type="button" name="button" className="btn btn-facebook">Facebook</button>
                  <button type="button" name="button" className="btn btn-google">Google</button>
                </div>
              </div>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Daftar</a>
              <div className="dropdown-menu dropdown-menu-right register" aria-labelledby="dropdownMenu">
                <div className="header-dropdown">
                  <h3>Gabung Sebagai Member</h3>
                </div>
                <div className="form-login">
                  <form className="" action="" method="post">
                    <label for="">Email</label>
                    <div className="form-inline">
                      <input type="email" name="" value="" className="form-control" placeholder="e.g email@example.com"/>
                      <button type="submit" name="button" className="btn btn-blue">Register</button>
                      <label for="">Data Anda dilindungi dan tidak akan disebarluaskan.</label>
                    </div>
                  </form>
                </div>
                <div className="other-log form-login">
                  <p className="text-center">Sudah memiliki akun? <a href="#" className="forgot">Log In <i className="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                </div>
                <div className="other-log bg-white">
                  <p className="text-center">Dengan melakukan pendaftaran, saya setuju dengan <a href="#" className="forgot">Kebijakan Privasi dan Syarat & Ketentuan <i className="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                </div>
                <div className="other-log text-left account-first">
                  <a href="#"><img src={fbsquare} className="img-fluid"/> Daftar melalui Facebook</a>
                </div>
                <div className="other-log text-left account-second">
                  <a href="#"><img src= {gpsquare}className="img-fluid"/> Daftar melalui Google</a>
                </div>
              </div>
            </li>
            <li className="lang">
              <ul className="list-unstyled">
                <i className="fa fa-caret-down" aria-hidden="true"></i>
                <li className="init"><img src={ind} className="img-fluid inline-block"/> IN</li>
                <li data-value="Indonesia" className="col-dark ina"><img src={ind} className="img-fluid inline-block"/> IN</li>
                <li data-value="English" className="col-dark eng"><img src={eng}className="img-fluid inline-block"/> EN</li>
              </ul>
            </li>
            <li className="lang">
              <ul className="list-unstyled-coin">
                <i className="fa fa-caret-down" aria-hidden="true"></i>
                <li className="init">IDR</li>
                <li data-value="idr" className="col-dark ina">IDR</li>
                <li data-value="usd" className="col-dark eng">USD</li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>



		)
}

export default Navbar