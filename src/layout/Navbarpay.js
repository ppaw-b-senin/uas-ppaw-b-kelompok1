import React from 'react';
import logo from '../img/logo.jpg';



const Navbarpay = () =>{
	return(
	
     <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <a className="navbar-brand" href="index.html">
          <img src={logo} alt="main logo" className="img-fluid"/>
        </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse checkout-step" id="navbarSupportedContent">
          <ul className="navbar-nav flex-row ml-md-auto d-md-flex">
            <li className="nav-item done">
              <a className="nav-link" href="#"><i className="fa fa-check-circle" aria-hidden="true"></i> Booking</a>
            </li>
            <li className="nav-item active">
              <a className="nav-link" href="#"><span className="number">2</span> Review</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#"><span className="number">3</span> Pembayaran</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#"><span className="number">4</span> Proses</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#"><span className="number">5</span>E-Tiket Terbit</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

		)
}

export default Navbarpay

