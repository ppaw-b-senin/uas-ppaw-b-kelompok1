import React from 'react';
import iconmap from '../img/icon-map.png';
import iconcc from '../img/icon-cc.png';
import iconshut from '../img/icon-shut.png';
import citilink from '../img/citilink.png';
import iconprofile from '../img/icon-profile.png';




const Booking = () =>{
	return(

		<div className="settings booking">
    <div className="container">
      <div className="row">
        <div className="col-md-4 col-12">
          <div className="sidebar navigate">
            <h1>NAVIGATE</h1>
            <h3>LOG IN / Register</h3>
            <ul className="navigation">
              <li><a href="#"><img src={iconprofile} className="img-fluid"/> Edit Profile</a></li>
              <li><a href="#"><img src={iconcc} className="img-fluid"/> Purchase List</a> <span className="count">1</span></li>
              <li><a href="#" className="active"><img src={iconmap} className="img-fluid"/> My Booking</a></li>
              <li><a href="#"><img src={iconshut} className="img-fluid"/> Log Out</a></li>
            </ul>
          </div>
        </div>
        <div className="col">
          <div className="booking-detail">
            <a href="#" className="blue"><i className="fa fa-long-arrow-left" aria-hidden="true"></i> Back</a>
            <div className="box-detail">
              <div className="title-box">
                <h4>Booking Details</h4>
              </div>
              <div className="inner-box">
                <div className="detail-booked">
                  <div className="name">
                    <span className="uptop">Booked by</span>
                    <p><strong>Novita Hidayati</strong></p>
                  </div>
                  <div className="name">
                    <span className="uptop">Booking Date</span>
                    <p><strong>11 Juni 2018</strong></p>
                  </div>
                  <div className="name">
                    <span className="uptop">Booking ID</span>
                    <p><strong>3145678990</strong></p>
                  </div>
                </div>
                <h4 className="bt">PAYMENT</h4>
                <div className="payment-box">
                  <div className="name">
                    <span className="uptop">Payment Methods</span>
                    <p><strong>Indomaret</strong></p>
                  </div>
                  <div className="name">
                    <span className="uptop">Payment Status</span>
                    <label className="label link">WAITING FOR PAYMENT PROOF</label>
                  </div>
                </div>
                <div className="action detail">
                  <button type="button" name="button" className="btn btn-blue">COMPLETE YOUR PAYMENT</button>
                </div>
              </div>
            </div>

            <div className="box-detail">
              <div className="title-box">
                <h4>Flight Details</h4>
              </div>
              <div className="inner-box">
                <div className="status-booking">
                  <div className="col-half">
                    <div className="logo-plane">
                      <img src={citilink} alt="Citilink" className="img-fluid"/>
                      <span>Citilink QG-980</span>
                    </div>
                    <div className="booking-code">
                      <p className="grey">PNR Code <span className="blue">VDDY6QS</span></p>
                    </div>
                  </div>
                  <div className="name">
                    <span className="uptop">Booking Status</span>
                    <label className="label link">WAITING FOR PAYMENT PROOF</label>
                  </div>
                </div>
                <div className="box-route">
                  <div className="block">
                    <p>Jakarta (CGK)</p>
                    <span className="desc">Soekarno Hatta International Airport</span>
                  </div>
                  <div className="divider">
                    <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                  </div>
                  <div className="block">
                    <p>Palembang (PLM)</p>
                    <span className="desc">Sultan Mahmud Badaruddin II</span>
                  </div>
                  <div className="divider middle">
                    <span className="height"></span>
                  </div>
                  <div className="block">
                    <p>Flight Shedule</p>
                    <span className="desc">Friday, 21 Dec 2018<br/> 06:00 - 07:15</span>
                  </div>
                </div>
                <div className="list-foot">
                  <p className="title">List of Passanger(s)</p>
                  <ol>
                    <li>Ms. Novita Hidayati</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


		)
}

export default Booking