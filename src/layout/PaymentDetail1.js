import React from 'react';
import citilink from '../img/citilink.png';



const PaymentDetail1 = () =>{
	return(

<React.Fragment>
  <div className="section checkout">
    <div className="container">
      <h1 className="title">Pembayaran</h1>
      <h2> Data Pemesanan</h2>
      <div className="row">
        <div className="col-md-8 col-12">
          <div className="content-wide-data">
            <div className="data-box">
              <div className="head-text">
                <h6 className="blue">Contact Details</h6>
              </div>
              <div className="detail-name">
                <strong className="name">Novita Hidayati</strong>
                <a href="#" className="blue">Details</a>
              </div>
            <div className="info">
              <div className="mobile">
                  <span className="block desc">Mobile Number</span>
                  <span className="block number">+6285795580512</span>
                </div>
                <div className="email">
                  <span className="block desc">Email</span>
                  <span className="block name">novitahidayati52@gmail.com</span>
                </div>
              </div>
            </div>
          </div>

          <div className="content-wide-data">
            <div className="data-box">
              <div className="head-text">
                <h6 className="blue">Treveler Details</h6>
              </div>
              <div className="detail-name">
                <strong className="name">Miss Novita Hidayati</strong>
                <a href="#" className="blue">Edit</a>
              </div>
            </div>
          </div>

          <div className="content-wide-data">
            <div className="data-box">
              <div className="head-text">
                <h6 className="blue">Flight Facilities</h6>
              </div>
              <div className="detail-name">
                <strong className="name">Baggage</strong>
                <a href="#" className="blue">Details</a>
              </div>
              <div className="info">
                <div className="mobile">
                  <span className="block name-info">Miss Novita Hidayati</span>
                  <span className="block destiny">CGK <i className="fa fa-long-arrow-right" aria-hidden="true"></i> PLM</span>
                  <select className="custom-select">
                    <option selected>20kg - Rp 0</option>
                    <option value="1">24kg - Rp 0</option>
                    <option value="2">30kg - Rp 0</option>
                    <option value="3">40kg - Rp 0</option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div className="content-wide-data">
            <div className="data-box refund-info">
              <div className="head-text">
                <h6 className="blue">Policy</h6>
              </div>
              <div className="detail-name">
                <strong className="name">Refund</strong>
                <label for="" className="green">Refundable</label>
                <p className="desc">Any cancellations or changes made to booking my be subjected to cancellation fee. Please make sure you have undestood the refund policy before requesting a refund</p>
                <a href="#" className="block blue read">Read More <i className="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
              <div className="detail-name">
                <strong className="name">Reschedule</strong>
                <label for="" className="green">Reschedule Available</label>
                <p className="desc">Thus flight can be rescheduled. Please make sure you have undestood the policy before requesting a reschedule</p>
                <a href="#" className="block blue read">Read More <i className="fa fa-long-arrow-right" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>

          <div className="content-wide-data">
            <div className="data-box">
              <div className="head-text">
                <h6 className="blue">Price Details</h6>
              </div>
              <div className="detail-name">
                <strong className="name">Price you pay</strong>
                <strong className="float-right">Rp. 437.672
                  <a className="blue ml-10 icon" data-toggle="collapse" href="#collapsePrice" role="button" aria-expanded="false" aria-controls="collapsePrice"><i className="fa fa-angle-down" aria-hidden="true"></i></a>
                </strong>
              </div>
              <div className="collapse" id="collapsePrice">
                <div className="card card-body bg-blue">
                  <p><i className="fa fa-info-circle" aria-hidden="true"></i> This account (Novita Hidayati (Google)) will earn 35 Points.</p>
                  <a href="#" className="blue">Learn more</a>
                </div>
              </div>
            </div>
          </div>
          
          <div className="action">
            <button type="button" className="btn btn-blue">Continue to Payment</button>
          </div>
        </div>
        <div className="col-md-4 col-12">
          <div className="box-payment-detail">
            <div className="content-wide-data">
              <div className="data-box">
                <div className="head-text">
                  <h6 className="blue">Contact Details</h6>
                </div>
                <div className="detail-name">
                  <i className="fa fa-plane" aria-hidden="true"></i> <span>Jakarta <i className="fa fa-long-arrow-right" aria-hidden="true"></i> Palembang</span>
                </div>
                <div className="inner-info collapse-acc">
                  <span className="name">Fri, 21 Dec 2018</span>
                  <a className="blue float-right" data-toggle="collapse" href="#collapseDetail" role="button" aria-expanded="false" aria-controls="collapseDetail"><i className="fa fa-angle-up" aria-hidden="true"></i></a>
                  <div className="collapse show" id="collapseDetail">
                    <div className="collapse-body">
                        <div className="info-plane">
                          <img src={citilink} alt=""/>
                          <div className="half">
                            <span className="name-plane">Citilink </span> <br/> <span className="type">Ekonomi</span>
                          </div>
                        </div>
                        <div className="list-route">
                          <ul>
                            <li className="from">
                              <div className="block">
                                <span className="boarding">17:40</span>
                                <span className="date">Jul 27, 2018</span>
                              </div>
                              <div className="block">
                                <span className="boarding">Jakarta (CKG)</span>
                                <span className="date">Bandara International Soekarno Hatta</span>
                              </div>
                            </li>
                            <li className="to">
                              <div className="block">
                                <span className="boarding">09:06</span>
                                <span className="date">Jul 28, 2018</span>
                              </div>
                              <div className="block">
                                <span className="boarding">Palembang (PLM)</span>
                                <span className="date">Sultan Mahmud Badarudin II</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <a href="#" className="blue">Details</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </React.Fragment>

		)
}

export default PaymentDetail1