import React from 'react';
import citilink from '../img/citilink.png';


const Order = () =>{
	return(



  <div className="order">
    <div className="container">
      <h3 className="title">Cek Pesanan</h3>
      <div className="code-book">
        <div className="form-group control-form">
          <label for="">Kode Booking <span className="red">*</span> </label>
          <input type="text" name="" value="" placeholder="Kode Booking" className="form-control"/>
          <button type="submit" name="button" className="btn btn-blue">CEK PESANAN</button>
        </div>

        <div className="booking-detail">
          <div className="box-detail">
            <div className="title-box">
              <h4>Booking Details</h4>
            </div>
            <div className="inner-box">
              <div className="detail-booked">
                <div className="name">
                  <span className="uptop">Booked by</span>
                  <p><strong>Novita Hidayati</strong></p>
                </div>
                <div className="name">
                  <span className="uptop">Booking Date</span>
                  <p><strong>11 Juni 2018</strong></p>
                </div>
                <div className="name">
                  <span className="uptop">Booking ID</span>
                  <p><strong>3145678990</strong></p>
                </div>
              </div>
              <h4 className="bt">PAYMENT</h4>
              <div className="payment-box">
                <div className="name">
                  <span className="uptop">Payment Methods</span>
                  <p><strong>Indomaret</strong></p>
                </div>
                <div className="name">
                  <span className="uptop">Payment Status</span>
                  <label className="label link bg-green-label">E-TICKET ISSUED</label>
                </div>
              </div>
            </div>
          </div>

          <div className="box-detail">
            <div className="title-box">
              <h4>Flight Details</h4>
            </div>
            <div className="inner-box">
              <div className="status-booking">
                <div className="col-half">
                  <div className="logo-plane">
                    <img src={citilink} alt="Citilink" className="img-fluid"/>
                    <span>Citilink QG-980</span>
                    <div className="name">
                      <span className="uptop">Booking Status</span>
                      <label className="label link bg-green-label">E-TICKED ISSUED</label>
                    </div>
                  </div>
                  <div className="booking-code order-last">
                    <p className="grey">PNR Code <span className="blue">VDDY6QS</span></p>
                    <div className="action detail">
                      <button type="button" name="button" className="btn btn-blue"><i className="fa fa-download" aria-hidden="true"></i> DOWNLOAD TICKET</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="box-route">
                <div className="block">
                  <p>Jakarta (CGK)</p>
                  <span className="desc">Soekarno Hatta International Airport</span>
                </div>
                <div className="divider">
                  <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                </div>
                <div className="block">
                  <p>Palembang (PLM)</p>
                  <span className="desc">Sultan Mahmud Badaruddin II</span>
                </div>
                <div className="divider middle">
                  <span className="height"></span>
                </div>
                <div className="block">
                  <p>Flight Shedule</p>
                  <span className="desc">Friday, 21 Dec 2018<br/> 06:00 - 07:15</span>
                </div>
              </div>
              <div className="list-foot">
                <p className="title">List of Passanger(s)</p>
                <ol>
                  <li>Ms. Novita Hidayati</li>
                </ol>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  

		)
}

export default Order