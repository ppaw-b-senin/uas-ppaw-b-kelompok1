import React from 'react';
import logofooter from '../img/logo-footer.jpg';
import logopartnerfooter from '../img/logo-partner-footer.jpg';


const Letter = () =>{
	return(



<React.Fragment>
<div className="newsletter">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-12">
            <div className="mail">
              <form className="" action="index.html" method="post">
                <div className="form-inline">
                  <label for="mail">SIGN UP FOR NEWS LETTER</label>
                  <input type="email" name="email" className="form-control" placeholder="Your email address"/>
                  <button type="submit" className="btn btn-blue" name="button">SUBSCRIBE</button>
                </div>
              </form>
            </div>
          </div>
          <div className="col-md-4 col-12">
            <div className="socmed">
              <ul>
                <li><a href="#facebook" className="fb"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#twitter" className="tw"><i className="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#gplus" className="gp"><i className="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#ig" className="ig"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#ytoube" className="yt"><i className="fa fa-youtube" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="widget-footer">
      <div className="container">
        <div className="row">
          <div className="col-md-3">
            <div className="widget-item">
              <a href="#"><img src={logofooter} alt="logo" className="img-fluid"/></a>
              <p>We provide Information Technology Services Maintenance which includes providing IT Hardware's Software and Networking</p>
            </div>
          </div>
          <div className="col-md-3">
            <div className="widget-item">
              <h2>Contact Us</h2>
              <ul>
                <li><a href="tel:+62212523820"><i className="fa fa-phone" aria-hidden="true"></i> 021-2523820</a></li>
                <li><a href="mailto:cs@pasarprodukbumn.com"><i className="fa fa-envelope" aria-hidden="true"></i>cs@pasarprodukbumn.com</a></li>
              </ul>
            </div>
          </div>
          <div className="col-md-3">
            <div className="widget-item">
              <h2>Customer Services</h2>
              <ul>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Site Map</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Privacy Policy</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Your Account</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Search Items</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Contact Us</a></li>
              </ul>
            </div>
          </div>
          <div className="col-md-3">
            <div className="widget-item">
              <h2>Payment & Shipping</h2>
              <ul>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Term Of Use</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Payment Methods</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Shipping Guide</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Location We Shop To</a></li>
                <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i>Estimated Delivery Time</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="copyright">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-12">
            <div className="text-copy">
              <p>&copy; COPYRIGHT 2018. <strong>PT. Rajawali Nusindo</strong> | All RIGHT RESERVED. </p>
            </div>
          </div>
          <div className="col-md col-12">
            <img src={logopartnerfooter} alt="card partner" className="img-fluid"/>
          </div>
        </div>
      </div>
    </div>
    </React.Fragment>

		)
}

export default Letter