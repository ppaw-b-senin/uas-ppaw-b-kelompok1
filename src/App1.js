import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Hero from './layout/Hero';
import Faq from './layout/Faq';
import Letter from './layout/Letter';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App1() {
  return (
    
    <React.Fragment>
      <Navbar/>
      <Hero/>
      <Faq/>
      <Letter/>
     
    </React.Fragment>
    
  );
}

export default App1;
