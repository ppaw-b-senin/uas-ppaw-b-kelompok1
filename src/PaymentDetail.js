import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Letter from './layout/Letter';
import PaymentDetail1 from './layout/PaymentDetail1';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function PaymentDetail() {
  return (
    
    <React.Fragment>
      <Navbar/>
    	<PaymentDetail1/>
   
      <Letter/>
    </React.Fragment>
    
  );
}

export default PaymentDetail;
