import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Hero from './layout/Hero';
import Letter from './layout/Letter';
import booking from './layout/booking';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App2() {
  return (
    
    <React.Fragment>
      <Navbar/>
      <Hero/>
      <booking/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default App2;
