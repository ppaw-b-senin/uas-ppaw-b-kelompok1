import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './layout/Navbar';
import Hero from './layout/Hero';
import Letter from './layout/Letter';
import Mabooking from './layout/Mabooking';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function Mybooking() {
  return (
    
    <React.Fragment>
      <Navbar/>
      <Hero/>
      <Mabooking/>
      <Letter/>
    </React.Fragment>
    
  );
}

export default Mybooking;
