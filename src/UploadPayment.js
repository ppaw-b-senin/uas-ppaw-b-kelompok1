import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Letter from './layout/Letter';
import Navbarup from './layout/Navbarup';
import UploadPayment1 from './layout/UploadPayment1';


import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function UploadPayment() {
  return ( 
    
    <React.Fragment>
      <Navbarup/>
    	<UploadPayment1/>

      <Letter/>
    </React.Fragment>
    
  );
}

export default UploadPayment
